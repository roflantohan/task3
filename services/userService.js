const { user } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll(){
        return UserRepository.getAll();
    }

    getById(id) {
        return UserRepository.getOne((obj) => obj.id === id);
    }

    search({email, password}) {
        return UserRepository.getOne((obj) => obj.email === email && obj.password === password)
    }

    getByEmail(email) {
        return UserRepository.getOne((obj) => obj.email === email);
    }

    getByPhone(phoneNumber) {
        return UserRepository.getOne((obj) => obj.phoneNumber === phoneNumber);
    }

    create(data) {
        return UserRepository.create(data)
    }

    update(id, data) {
        return UserRepository.update(id, data)
    }

    delete(id) {
        return UserRepository.delete(id)
    }

    
}

module.exports = new UserService();