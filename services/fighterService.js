const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getAll(){
        return FighterRepository.getAll();
    }

    getByName(name) {
        return FighterRepository.getOne((obj) => obj.name === name);
    }

    getById(id) {
        return FighterRepository.getOne((obj) => obj.id === id);
    }

    create(data) {
        return FighterRepository.create(data)
    }

    update(id, data){
        return FighterRepository.update(id, data)
    }

    delete(id){
        return FighterRepository.delete(id)
    }
}

module.exports = new FighterService();