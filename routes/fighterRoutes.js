const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const e = require('express');

const router = Router();

router.get("/", responseMiddleware)

router.post("/", createFighterValid, responseMiddleware)

router.get('/:id', responseMiddleware)

router.delete("/:id", responseMiddleware)

router.put("/:id", updateFighterValid, responseMiddleware)

module.exports = router;