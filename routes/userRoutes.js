const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get("/:id",  responseMiddleware);

router.delete("/:id",  responseMiddleware)

router.put("/:id",  updateUserValid, responseMiddleware)

router.get("/",  responseMiddleware)

router.post("/", createUserValid, responseMiddleware)
    

module.exports = router;